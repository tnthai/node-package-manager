export const PackageModule = Object.freeze({
  Ui: "ui",
  NpmMarket: "market_packages",
  Global: {
    name: "global_packages",
    get Configuration() {
      return `${this.name}/configuration`;
    },
    toString: function() {
      return this.name;
    }
  },
  Local: {
    name: "local_packages",
    get SelectedItem() {
      return `${this.name}/selectedItem`;
    },
    toString: function() {
      return this.name;
    }
  },
  PackageInfo: "package_info"
});

export const DependencyTypes = Object.freeze({
  Normal: "dependencies",
  Dev: "devDependencies",
  Optional: "optionalDependencies"
});
