import toast from "izitoast";

export function showWarning(message) {
  toast.show({
    title: "Warning",
    messageColor: "white",
    message,
    backgroundColor: "#FFCC80",
    timeout: 3000
  });
}

export function showSuccess(message) {
  toast.show({
    title: "Success",
    messageColor: "white",
    message,
    backgroundColor: "#A5D6A7",
    timeout: 3000
  });
}

export function showError(message) {
  toast.show({
    title: "Error",
    messageColor: "white",
    message,
    backgroundColor: "#e57373",
    timeout: 3000
  });
}
