import shell from "shelljs";
import jsonfile from "jsonfile";
import fs from "fs-jetpack";
import request from "request";
import { appConfig } from "../config";
import { orderBy } from "lodash";
import { ipcRenderer } from "electron";
import readNpmJson from "read-package-json";
import path from "path";
import { getInstallOptions, yarnInstalled } from "./application";
import { assert } from "chai";

export function timeoutPromise(promise$, timeout) {
  assert.instanceOf(promise$, Promise, "Invalid input promise!");
  assert.isAbove(timeout, 0, "Invalid input timeout");

  let timeout$ = new Promise((resolve, reject) => {
    let timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      reject(new Error("Operation timeout!"));
    }, timeout);
  });

  return Promise.race([promise$, timeout$]);
}

export function execCommandAsync(cmd, timeout = 0) {
  let main$ = new Promise((resolve, reject) => {
    shell.exec(cmd, (code, stdout, stderr) => {
      if (code !== 0 && stderr) {
        console.error("Error on executing cmd ", stderr);
        reject(new Error(stderr));
      } else {
        resolve(stdout);
      }
    });
  });

  return timeout > 0 ? timeoutPromise(main$, timeout) : main$;
}

export function writeJsonFileAsync(path, data, options) {
  return new Promise((resolve, reject) => {
    jsonfile.writeFile(path, data, options, (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject(new Error(err));
      }
    });
  });
}

export function readJsonFileAsync(path, options) {
  options = options || {};
  const isNpm = options.isNpm || false;
  const strictNpm = options.strictNpm || false;
  const fallbackResult = options.fallbackResult;

  return new Promise((resolve, reject) => {
    fs.existsAsync(path).then(result => {
      if (!result) {
        if (!fallbackResult) {
          reject(new Error("File doesn't exist"));
        } else {
          resolve(fallbackResult);
        }
      } else {
        const checkResult = (err, data) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(data);
          }
        };

        if (isNpm) {
          readNpmJson(path, console.warn, strictNpm, (err, data) => {
            checkResult(err, data);
          });
        } else {
          jsonfile.readFile(path, (err, data) => {
            checkResult(err, data);
          });
        }
      }
    });
  });
}

export function getPackageDetailAsync(name, version) {
  return new Promise((resolve, reject) => {
    request.get(
      `${appConfig.RegistryUrl}${name}`,
      {
        json: true
      },
      (err, resp, body) => {
        if (resp.statusCode === 200) {
          let result = Object.assign({}, body, {
            versions: getVersionPackage(body.versions, version)
          });
          resolve(result);
        } else {
          let error = err
            ? err
            : `The response returned with status code ${
                resp.statusCode
              } and message ${resp.statusMessage}`;
          reject(new Error(error));
        }
      }
    );
  });
}

export function getUserDataPathAsync() {
  return new Promise(resolve => {
    ipcRenderer.send("onRendererMessage", { type: "getUserDataPath" });
    ipcRenderer.once("ON_APP_USERDATA_RETURN", (e, arg) => {
      resolve(arg);
    });
  });
}

export function chooseDirectoryAsync(processId) {
  return new Promise(resolve => {
    ipcRenderer.send("onRendererMessage", {
      id: processId,
      type: "openDirChoosingDialog"
    });

    ipcRenderer.once(`${processId}_ON_DIR_CHOOSING_RETURN`, (e, arg) => {
      resolve(arg);
    });
  });
}

export function installPackageFolderAsync(path) {
  shell.cd(path);
  let installCmd = shell.which("yarn") ? "yarn install" : "npm install";
  return execCommandAsync(installCmd);
}

export function uninstallPackageAsync(folderPath, name) {
  shell.cd(folderPath);
  return fs
    .existsAsync(path.resolve(folderPath, "yarn.lock"))
    .then(lockFileExist => {
      let cmd =
        !lockFileExist || !yarnInstalled()
          ? `npm uninstall ${name}`
          : `yarn remove ${name}`;
      return execCommandAsync(cmd);
    });
}

export function installPackageAsync(folderPath, name, options) {
  shell.cd(folderPath);
  return fs
    .existsAsync(path.resolve(folderPath, "yarn.lock"))
    .then(lockFileExist => {
      options = getInstallOptions(options);
      let cmd =
        lockFileExist && yarnInstalled()
          ? `yarn add ${name} ${options}`
          : `npm install ${name} ${options}`;
      return execCommandAsync(cmd);
    });
}

export function requestAsync(url, options) {
  return new Promise((resolve, reject) => {
    request.get(url, options, (err, resp, body) => {
      if (err) {
        reject(new Error(err));
      } else {
        resolve({
          resp,
          body
        });
      }
    });
  });
}

export function checkPackagesAsync(options) {
  const { path, useTimeout } = options || {};
  return new Promise((resolve, reject) => {
    let cmdExecutor = () => {
      if (path) {
        shell.cd(path);
        return execCommandAsync("npm outdated --json", useTimeout ? 10000 : 0);
      }

      return execCommandAsync("npm outdated -g --json");
    };

    cmdExecutor()
      .then(res => {
        resolve(res ? JSON.parse(res.trim()) : {});
      })
      .catch(err => {
        reject(new Error(err));
      });
  });
}

function getVersionPackage(versions, version) {
  if (!versions) {
    return {};
  }

  if (version) {
    return versions[version];
  }

  let keys = orderBy(Object.keys(versions), x => x, ["desc"]);
  return versions[keys[0]] || {};
}

export function async(generator) {
  let iterator = generator();

  function handle(iteratorRes) {
    if (iteratorRes.done) {
      return;
    }

    let iteratorVal = iteratorRes.value;
    if (iteratorVal instanceof Promise) {
      iteratorVal
        .then(res => handle(iterator.next(res)))
        .catch(err => iterator.throw(err));
    }
  }

  try {
    handle(iterator.next());
  } catch (err) {
    iterator.throw(err);
  }
}

export function untilFileExist(location) {
  new Promise(resolve => {
    let pkgJsonExist = false;
    while (!pkgJsonExist) {
      pkgJsonExist = fs.exists(location);
    }

    resolve();
  });
}
