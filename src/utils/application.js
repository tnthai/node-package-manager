import { appConfig } from "../config";
import shell from "shelljs";
import Vue from "vue";
import { ipcRenderer } from "electron";

const NpmToYarnInstallOptions = Object.freeze({
  "--save": "",
  "--save-dev": "-D",
  "--save-optional": "-O"
});

export function getPackageHomePage(name) {
  if (name.indexOf("/") > -1) {
    let names = name.split("/");
    return `${appConfig.NpmPackagePage}${names[0]}/${names[1]}`;
  }

  return `${appConfig.NpmPackagePage}${name}`;
}

export function getRandomColor() {
  return (
    "#" +
    Math.random()
      .toString(16)
      .substr(-6)
  );
}

export function getInstallOptions(options) {
  let yarnOpts = NpmToYarnInstallOptions[options];
  return yarnInstalled() ? (yarnOpts ? yarnOpts : options) : options;
}

export function yarnInstalled() {
  return shell.which("yarn");
}

export function vueSetObjectProperty(object, key, value) {
  if (key in object) {
    object[key] = value;
  } else {
    Vue.set(object, key, value);
  }
}

export function vueDeleteObjectProperty(object, key) {
  Vue.delete(object, key);
}

export function initRightClickContextMenu() {
  window.addEventListener(
    "contextmenu",
    e => {
      e.preventDefault();
      ipcRenderer.send("onRendererMessage", {
        type: "contextmenu"
      });
    },
    false
  );
}
