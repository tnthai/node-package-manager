import chokidar from "chokidar";
import validatePkgName from "validate-npm-package-name";

export const ChangeMode = Object.freeze({
  Add: 0,
  Remove: 1
});

class PackageWatcher {
  static Listeners = [];

  listen(location, onChangeCallback) {
    if (!this.locationExist(location)) {
      const watcher = chokidar.watch(location, {
        depth: 0
      });

      PackageWatcher.Listeners.push({ location, watcher });

      watcher.on("ready", () => {
        console.log("Ready for watching incoming changes at ", location);
        watcher
          .on("addDir", path => {
            console.log(`Directory ${path} has been added at `, location);
            const mode = ChangeMode.Add;
            const { result: validationResult, name } = this.validatePackagePath(
              path
            );
            if (validationResult) {
              onChangeCallback(null, { path, mode, name });
            }
          })
          .on("unlinkDir", path => {
            console.log(`Directory ${path} has been removed at `, location);
            const mode = ChangeMode.Remove;
            const { result: validationResult, name } = this.validatePackagePath(
              path
            );
            if (validationResult) {
              onChangeCallback(null, { path, mode, name });
            }
          })
          .on("error", error => {
            console.error("Error while watching at ", location);
            onChangeCallback(error);
          });
      });
    }
  }

  validatePackagePath(pkgPath) {
    if (!pkgPath) {
      return { result: false };
    }

    const pathSegments = pkgPath.split("\\");
    const pkgName = pathSegments[pathSegments.length - 1];
    const pgkNameValidationResult = validatePkgName(pkgName);

    const { errors } = pgkNameValidationResult;
    if (errors && errors.length > 0) {
      return { result: false };
    }

    return { result: true, name: pkgName };
  }

  locationExist(location) {
    return PackageWatcher.Listeners.map(l => l.location).indexOf(location) > -1;
  }

  release(location) {
    const watcherObj = PackageWatcher.Listeners.find(
      l => l.location === location
    );
    if (watcherObj && watcherObj instanceof chokidar.FSWatcher) {
      watcherObj.close();
    }
  }
}

const packageWatcher = new PackageWatcher();
export default packageWatcher;
