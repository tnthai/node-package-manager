import "./styles/app.scss";
import Vue from "vue";
import Vuetify from "vuetify";
import { AppContainer } from "./components/app-container";
import store from "./store";
import router from "./router";
import { sync } from "vuex-router-sync";
import registerControls from "./controls";
import registerDirectives from "./directives";

sync(store, router);
Vue.use(Vuetify);
registerControls(Vue);
registerDirectives(Vue);

new Vue({
  el: "#app",
  components: {
    AppContainer
  },
  store,
  router
});
