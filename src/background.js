// This is main process of Electron, started as first thing when your
// app starts. It runs through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

import path from "path";
import url from "url";
import { app, dialog, ipcMain } from "electron";
import createWindow, { ContextMenu } from "./utils/window";
import shell from "shelljs";

const env = require(`../config/env_${process.env.NODE_ENV}.json`);

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}

app.on("ready", () => {
  if (!shell.which("npm")) {
    dialog.showErrorBox(
      "Error",
      "NodeJs is not on your computer. Follow this link https://nodejs.org/en/download/ and then come back later"
    );
  } else {
    const mainWindow = createWindow("main", {
      width: 1500,
      height: 800,
      minWidth: 1200,
      icon: path.resolve(__dirname, "../assets/icons/nodejs_logo.png")
    });

    mainWindow.loadURL(
      url.format({
        pathname: path.resolve(__dirname, "../dist/index.html"),
        protocol: "file:",
        slashes: true
      })
    );

    installExtensions();
    const contextMenu = new ContextMenu(mainWindow);
    ipcMain.on("onRendererMessage", (e, arg) => {
      switch (arg.type) {
        case "openDirChoosingDialog":
          dialog.showOpenDialog(
            mainWindow,
            {
              properties: ["openDirectory"]
            },
            res => {
              e.sender.send(`${arg.id}_ON_DIR_CHOOSING_RETURN`, res);
            }
          );
          break;
        case "getUserDataPath":
          e.sender.send("ON_APP_USERDATA_RETURN", app.getPath("userData"));
          break;
        case "contextmenu":
          contextMenu.show();
          break;
        default:
          break;
      }
    });
  }
});

app.on("window-all-closed", () => {
  app.quit();
});

function installExtensions() {
  let installExtension = require("electron-devtools-installer");
  installExtension
    .default(installExtension.VUEJS_DEVTOOLS)
    .then(() => {
      console.log("Install `vue-devtools` successfully!");
    })
    .catch(err => {
      console.log("Unable to install `vue-devtools`: ", err);
    });
}
