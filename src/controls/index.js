import { NavigationBar } from "./NavigationBar";
import { SimpleChips } from "./SimpleChips";
import { ActionButton } from "./ActionButton";
import { PackageSearch } from "./PackageSearch";
import { DataTable } from "./DataTable";

export default function registerControls(vueEngine) {
  vueEngine.component("nav-bar", NavigationBar);
  vueEngine.component("chips", SimpleChips);
  vueEngine.component("action-button", ActionButton);
  vueEngine.component("package-search", PackageSearch);
  vueEngine.component("nt-datatable", DataTable);
}
