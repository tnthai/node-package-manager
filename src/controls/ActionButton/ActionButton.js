import Vue from "vue";
import Component from "vue-class-component";

@Component({
  template: require("./ActionButton.html"),
  props: {
    title: String,
    disabled: Boolean,
    type: String,
    color: {
      type: String,
      default: "teal"
    },
    buttonCls: {
      type: String,
      default: ""
    },
    status: {
      type: String,
      default: ""
    }
  }
})
export class ActionButton extends Vue {
  buttonStatus = "";

  get buttonColor() {
    switch (this.buttonStatus) {
      case "confirming":
        return "red";
      default:
        return this.color;
    }
  }

  get buttonTitle() {
    switch (this.buttonStatus) {
      case "confirming":
        return "Are you sure?";
      default:
        return this.title;
    }
  }

  onBtnClick(e) {
    if (this.type === "delete" && !this.buttonStatus) {
      this.buttonStatus = "confirming";
    } else {
      this.buttonStatus = "";
      this.$emit("onClick", e);
    }
  }

  onBtnClickOutside(e) {
    this.buttonStatus = "";
  }
}
