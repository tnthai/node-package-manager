import Vue from "vue";
import Component from "vue-class-component";
import classnames from "classnames";
import { mapGetters } from "vuex";
import { PackageModule } from "../../constants";
import { isFunction } from "lodash";

export const ActionVisibleMode = {
  Always: "always",
  OnHover: "onHover",
  OnSelected: "onSelected",
  OnTouch: "onTouch"
};

@Component({
  template: require("./data-table.html"),
  props: {
    columns: {
      type: Array,
      default: [],
      required: true
    },
    className: {
      type: String,
      default: ""
    },
    items: {
      type: Array,
      default: [],
      required: true
    },
    itemKey: {
      type: String,
      default: ""
    },
    clickableRow: {
      type: Boolean,
      default: true
    },
    loading: {
      type: Boolean,
      default: false
    },
    pagination: {
      type: Object,
      default: null
    }
  },
  computed: mapGetters({
    windowResizing: `${PackageModule.Ui}/windowResizing`
  })
})
export class DataTable extends Vue {
  extraHeadVisible = false;
  localPagination = {
    pageIndex: 1,
    pageSize: 10
  };

  updated() {
    this.$nextTick(() => {
      const { hasVerticalScrollbar } = divHasScrollbars(
        this.$refs["table-body"]
      );

      this.extraHeadVisible = hasVerticalScrollbar;
    });
  }

  get tableItems() {
    if (!this.pagination) {
      const { pageIndex, pageSize } = this.localPagination;
      return this.items.slice((pageIndex - 1) * pageSize, pageIndex * pageSize);
    }

    return this.items;
  }

  get tableClassName() {
    return classnames({
      "nt-table": true,
      [this.className]: this.className != undefined
    });
  }

  get pageSize() {
    return this.pagination
      ? this.pagination.pageSize
      : this.localPagination.pageSize;
  }

  get pageIndex() {
    return this.pagination
      ? this.pagination.pageIndex
      : this.localPagination.pageIndex;
  }

  get totalPages() {
    let totalCount = this.pagination
      ? this.pagination.totalCount
      : this.items.length;
    return Math.floor(totalCount / this.pageSize) + 1;
  }

  getHeaderClassName(col) {
    return classnames({
      "nt-table-head": true,
      [col.headerClassName]: col.headerClassName != undefined,
      [`flex-${col.columnFlex}`]: col.columnFlex && col.columnFlex > 0,
      "md-box-pack-center": col.columnCenter
    });
  }

  getBodyRowClassName(item) {
    return classnames({
      "nt-table-row": true,
      pointer: this.clickableRow
    });
  }

  getBodyRowId(item) {
    return `row-${item[this.itemKey]}`;
  }

  getDataCellClassName(col, item) {
    const { OnHover, OnSelected, OnTouch } = ActionVisibleMode;
    return classnames({
      "nt-table-data": true,
      "col-action": col.columnName === "action",
      [OnHover]:
        col.columnName === "action" &&
        (col.visibleMode === OnHover || col.visibleMode === OnTouch),
      [OnSelected]:
        col.columnName === "action" &&
        (col.visibleMode === OnSelected || col.visibleMode === OnTouch),
      [col.cellClassName]: col.cellClassName != undefined,
      [item.className]: item.className != undefined,
      [`flex-${col.columnFlex}`]: col.columnFlex && col.columnFlex > 0,
      "md-box-pack-center": col.columnCenter
    });
  }

  onPageChange(page) {
    if (!this.pagination || !isFunction(this.pagination.onPageChange)) {
      this.localPagination.pageIndex = page;
    } else {
      this.pagination.onPageChange(page);
    }
  }
}

function divHasScrollbars(div) {
  let result = {
    hasVerticalScrollbar: div && div.scrollHeight > div.clientHeight,
    hasHorizontalScrollbar: div && div.scrollWidth > div.clientWidth
  };

  return result;
}
