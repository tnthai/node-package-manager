import Vue from "vue";
import Component from "vue-class-component";
import { Watch } from "vue-property-decorator";
import { async, requestAsync } from "utils/promises";
import { isString } from "lodash";
import { appConfig } from "config";

@Component({
  template: require("./package-search.html"),
  props: {
    label: {
      type: String,
      default: "Search a package"
    },
    appendIcon: {
      type: String,
      default: "file_download"
    }
  }
})
export class PackageSearch extends Vue {
  loading = false;
  select = "";
  items = [];
  search = "";
  timeOutId = 0;

  get selectModel() {
    return this.select;
  }

  set selectModel(val) {
    this.select = val;
    this.$emit("onSelect", val);
  }

  @Watch("search")
  onSearchChanged(val, oldVal) {
    clearTimeout(this.timeOutId);
    if (!isString(val) || val.trim().length < 3) {
      return;
    }

    this.timeOutId = setTimeout(() => {
      this.querySelections(val.trim());
      clearTimeout(this.timeOutId);
    }, 1000);
  }

  querySelections(val) {
    let me = this;
    async(function*() {
      me.loading = true;
      const url = `${
        appConfig.RegistrySearchUrl
      }?text=${val}&from=${0}&size=${10}`;

      let { body } = yield requestAsync(url, {
        json: true
      });

      me.items = body.objects.map(item => item.package.name);
      me.loading = false;
    });
  }
}
