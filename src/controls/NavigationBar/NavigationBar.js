import Vue from "vue";
import Component from "vue-class-component";
import { capitalize } from "lodash";
import { mapGetters } from "vuex";

@Component({
  template: require("./NavigationBar.html"),
  props: {
    actionMenus: {
      type: Array
    }
  },
  computed: mapGetters(["appRoute"])
})
export class NavigationBar extends Vue {
  get routeItems() {
    return ["Home"].concat(
      this.appRoute.path
        .split("/")
        .filter(item => item !== "")
        .map(item => capitalize(item))
    );
  }
}
