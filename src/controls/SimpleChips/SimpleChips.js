import Vue from "vue";
import Component from "vue-class-component";
import { uniqBy } from "lodash";

@Component({
  template: require("./SimpleChips.html"),
  props: {
    items: {
      type: Array
    },
    containerCls: {
      type: String
    }
  }
})
export class SimpleChips extends Vue {
  get uniqItems() {
    return uniqBy(this.items, "name");
  }
}
