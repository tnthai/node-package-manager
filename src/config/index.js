import RegClient from "npm-registry-client";

export const appConfig = {
  NpmPackagePage: "https://www.npmjs.com/package/",
  RegistryUrl: "https://registry.npmjs.org/",
  RegistrySearchUrl: "https://registry.npmjs.org/-/v1/search"
};

export const regClient = new RegClient();
