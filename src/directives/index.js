import { clickOutsideDirective } from "./click-outside";

export default function registerDirectives(vueEngine) {
  vueEngine.directive("clickoutside", clickOutsideDirective);
}
