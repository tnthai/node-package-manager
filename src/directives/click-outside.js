export const clickOutsideDirective = {
  name: "clickoutside",
  inserted(el, binding) {
    const onClick = e => execute(e, el, binding);
    document.body.addEventListener("click", onClick);
    el._clickOutside = onClick;
  },

  unbind(el) {
    document.body.removeEventListener("click", el._clickOutside);
    delete el._clickOutside;
  }
};

function execute(e, el, binding) {
  if (!isClickedInElement(e, el)) {
    requestAnimationFrame(() => binding.value());
  }
}

function isClickedInElement(e, el) {
  const { clientX: x, clientY: y } = e;
  const b = el.getBoundingClientRect();
  return x >= b.left && x <= b.right && y >= b.top && y <= b.bottom;
}
