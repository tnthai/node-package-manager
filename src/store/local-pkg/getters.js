import { isObject } from "lodash";

const getters = {
  localPackageItems: state =>
    state.packages.items.map(item => ({
      version: item.version,
      license: item.license,
      numDeps:
        Object.keys(item.dependencies).length +
        Object.keys(item.devDependencies).length,
      numScripts: Object.keys(item.scripts).length,
      author: isObject(item.author) ? item.author.name : item.author,
      name: item.name
    })),
  selectedPackage: state => state.selectedItem.data,
  isAppLoading: state => state.loading,
  localPackagePathItems: state => state.packages.paths
};

export default getters;
