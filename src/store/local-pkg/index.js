import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import selectedStore from "./selected-item";

const store = {
  namespaced: true,
  state: {
    packages: {
      items: [],
      paths: []
    },
    loading: false
  },
  modules: {
    selectedItem: selectedStore
  },
  getters,
  actions,
  mutations
};

export default store;
