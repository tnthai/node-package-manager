import {
  readJsonFileAsync,
  writeJsonFileAsync,
  getUserDataPathAsync,
  chooseDirectoryAsync,
  installPackageFolderAsync,
  async,
  checkPackagesAsync
} from "utils/promises";
import { MutationTypes } from "./mutations";
import { showError, showWarning } from "utils/ui-notification";
import { isArray, isEmpty } from "lodash";
import fs from "fs-jetpack";
import path from "path";
import { PackageModule } from "../../constants";

const LocalSelectedItem = PackageModule.Local.SelectedItem;

const actions = {
  fetchPackageFolders({ dispatch, commit, rootState }) {
    async(function*() {
      commit(MutationTypes.FETCH_DATA);

      let location = rootState.ui.appPaths.userData;
      if (!location) {
        dispatch("ui/getUserDataPath", null, { root: true });
        loadPackageFolderByLocation(commit, yield getUserDataPathAsync());
      } else {
        loadPackageFolderByLocation(commit, location);
      }
    });
  },

  addPackageFolder({ commit, rootState }) {
    async(function*() {
      try {
        let paths = yield chooseDirectoryAsync("LOAD_PACKAGE_FOLDER");
        if (!paths) {
          return;
        }

        commit(MutationTypes.FETCH_DATA);
        let projectpath = paths[0].trim();
        let promiseValues = yield Promise.all([
          readJsonFileAsync(path.resolve(projectpath, "package.json"), {
            isNpm: true
          }),
          installPackageFolderAsync(projectpath)
        ]);

        commit(MutationTypes.ADD_PACKAGE_FOLDER_SUCCEED, {
          result: promiseValues[0],
          path: { path: projectpath, name: promiseValues[0].name },
          localPkgPath: path.resolve(
            rootState.ui.appPaths.userData,
            "local-packages.json"
          )
        });
        commit(MutationTypes.DONE_FETCHING_DATA);
      } catch (err) {
        showError("Something wrong when uploading package folder!");
        commit(MutationTypes.DONE_FETCHING_DATA);
      }
    });
  },

  selectPkg({ dispatch, commit, state }, name) {
    async(function*() {
      commit(MutationTypes.FETCH_DATA);
      let itemIndex = state.packages.paths.findIndex(p => p.name === name);
      let selectedPath = state.packages.paths[itemIndex];
      let selectedItem = state.packages.items[itemIndex];
      selectedItem.locationPath = selectedPath ? selectedPath.path : "";

      try {
        selectedItem.packageStatus = yield checkPackagesAsync({
          path: selectedItem.locationPath,
          useTimeout: true
        });
      } finally {
        dispatch(`${LocalSelectedItem}/setData`, selectedItem, {
          root: true
        });
        commit(MutationTypes.DONE_FETCHING_DATA);
      }
    });
  },

  unselectPkg({ dispatch }) {
    dispatch(
      `${LocalSelectedItem}/setData`,
      {},
      {
        root: true
      }
    );
  },

  removeSelectedPackage({ commit, state, rootState, dispatch }) {
    let selectedPkg = state.selectedItem.data;
    if (!selectedPkg) {
      return;
    }

    commit(MutationTypes.REMOVE_PACKAGE, {
      name: selectedPkg.name,
      localPkgPath: path.resolve(
        rootState.ui.appPaths.userData,
        "local-packages.json"
      )
    });
    dispatch(
      `${LocalSelectedItem}/setData`,
      {},
      {
        root: true
      }
    );
  },

  reloadSelectedFolderPackage({ commit, state, rootState, dispatch }) {
    async(function*() {
      dispatch(`${LocalSelectedItem}/fetchData`, undefined, {
        root: true
      });
      try {
        const itemIndex = state.packages.paths.findIndex(
          p => p.name === state.selectedItem.data.name
        );
        let selectedPathItem = state.packages.paths[itemIndex];
        let selectedItem = state.packages.items[itemIndex];

        let isLocExists = yield fs.existsAsync(
          path.resolve(selectedPathItem.path, "package.json")
        );

        if (!isLocExists) {
          commit(MutationTypes.REMOVE_PACKAGE, {
            name: selectedItem.name,
            localPkgPath: path.resolve(
              rootState.ui.appPaths.userData,
              "local-packages.json"
            )
          });
          dispatch(
            `${LocalSelectedItem}/setData`,
            {},
            {
              root: true
            }
          );
          showWarning("Current location no longer exists! Package removed!");
        } else {
          let promiseValues = yield Promise.all([
            readJsonFileAsync(
              path.resolve(selectedPathItem.path, "package.json"),
              {
                isNpm: true
              }
            ),
            installPackageFolderAsync(selectedPathItem.path)
          ]);

          let newPkg = promiseValues[0];
          newPkg.locationPath = selectedPathItem.path;
          newPkg.packageStatus = yield checkPackagesAsync({
            path: selectedPathItem.path
          });
          commit(MutationTypes.SET_PACKAGE, newPkg);
          dispatch(`${LocalSelectedItem}/setData`, newPkg, {
            root: true
          });
        }
      } catch (err) {
        showError("Error occured when refreshing. Abort ...");
      } finally {
        dispatch(`${LocalSelectedItem}/doneFetchingData`, undefined, {
          root: true
        });
      }
    });
  }
};

function loadPackageFolderByLocation(commit, location) {
  async(function*() {
    const packagesPath = path.resolve(location, "local-packages.json");
    let isFileExists = yield fs.existsAsync(packagesPath);
    if (!isFileExists) {
      yield writeJsonFileAsync(packagesPath, []);
      commit(MutationTypes.SET_PACKAGES, { items: [], paths: [] });
      commit(MutationTypes.DONE_FETCHING_DATA);
    } else {
      let pkgLocations = yield readJsonFileAsync(packagesPath);
      if (!isArray(pkgLocations)) {
        commit(MutationTypes.DONE_FETCHING_DATA);
        return;
      }

      let pkgs$ = pkgLocations.map(p =>
        readJsonFileAsync(path.resolve(p.path, "package.json"), {
          isNpm: true,
          fallbackResult: {}
        })
      );

      let packagesContents = yield Promise.all(pkgs$);
      commit(MutationTypes.SET_PACKAGES, {
        items: packagesContents.filter(p => !isEmpty(p)),
        paths: pkgLocations
          .filter((item, index) => !isEmpty(packagesContents[index]))
          .map((item, index) => ({
            path: item.path,
            name: packagesContents[index].name
          }))
      });
      commit(MutationTypes.DONE_FETCHING_DATA);
    }
  });
}

export default actions;
