import { showError } from "utils/ui-notification";
import { writeJsonFileAsync } from "utils/promises";
import { assert } from "chai";
import { remove } from "lodash";

export const MutationTypes = {
  FETCH_DATA: "LOCAL_FETCH_DATA",
  DONE_FETCHING_DATA: "LOCAL_DONE_FETCHING_DATA",
  ADD_PACKAGE_FOLDER_SUCCEED: "LOCAL_ADD_PACKAGE_FOLDER_SUCCEED",
  SET_PACKAGES: "LOCAL_SET_PACKAGES",
  SET_PACKAGE: "LOCAL_SET_PACKAGE",
  REMOVE_PACKAGE: "LOCAL_REMOVE_PACKAGE"
};

const mutations = {
  [MutationTypes.FETCH_DATA]: (state, val) => {
    state.loading = true;
  },

  [MutationTypes.DONE_FETCHING_DATA]: (state, val) => {
    state.loading = false;
  },

  [MutationTypes.ADD_PACKAGE_FOLDER_SUCCEED]: (
    state,
    { result, path, localPkgPath }
  ) => {
    if (state.packages.items.some(item => item.name === result.name)) {
      showError(`Duplicate name when adding package: ${result.name}`);
    } else {
      state.packages.items.push(result);
      state.packages.paths.push(path);

      assert.equal(
        state.packages.items.length,
        state.packages.paths.length,
        "Fatal! Local packages's length mismatch!"
      );

      writeJsonFileAsync(localPkgPath, state.packages.paths).then(res => {
        console.log("Writing uploaded package successfully");
      });
    }
  },

  [MutationTypes.SET_PACKAGES]: (state, payload) => {
    state.packages.items = payload.items;
    state.packages.paths = payload.paths;

    assert.equal(
      state.packages.items.length,
      state.packages.paths.length,
      "Fatal! Local packages's length mismatch!"
    );
  },

  [MutationTypes.REMOVE_PACKAGE]: (state, { name, localPkgPath }) => {
    remove(state.packages.items, p => p.name === name);
    remove(state.packages.paths, p => p.name === name);
    writeJsonFileAsync(localPkgPath, state.packages.paths).then(res => {
      console.log("Writing updated packages successfully");
    });
  },

  [MutationTypes.SET_PACKAGE]: (state, payload) => {
    const { items } = state.packages;
    state.packages.items = items.map(
      i => (i.name === payload.name ? payload : i)
    );
  }
};

export default mutations;
