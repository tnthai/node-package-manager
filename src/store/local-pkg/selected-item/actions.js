import {
  uninstallPackageAsync,
  async,
  execCommandAsync,
  readJsonFileAsync,
  writeJsonFileAsync,
  installPackageAsync
} from "utils/promises";
import { showSuccess, showError } from "utils/ui-notification";
import { MutationTypes } from "./mutations";
import shell from "shelljs";
import path from "path";
import { PackageModule } from "../../../constants";

const actions = {
  fetchData({ commit }) {
    commit(MutationTypes.FETCH_DATA);
  },

  doneFetchingData({ commit }) {
    commit(MutationTypes.DONE_FETCHING_DATA);
  },

  setData({ commit }, data) {
    commit(MutationTypes.SET_DATA, data);
  },

  uninstallPackage({ commit, state }, { pkgName, type }) {
    async(function*() {
      const { name, locationPath } = state.data;
      commit(MutationTypes.START_UNINSTALL_PACKAGE, pkgName);

      try {
        yield uninstallPackageAsync(locationPath, pkgName);
        showSuccess(`Uninstall ${pkgName} from ${name} successfully!`);
        commit(MutationTypes.UNINSTALL_PACKAGE_SUCCEED, {
          pkgName,
          type
        });
      } catch (err) {
        let msg = `Fail to uninstall ${pkgName} in ${name}!`;
        console.error(msg, err);
        commit(MutationTypes.DONE_UNINSTALL_PACKAGE, pkgName);
        showError(msg);
      }
    });
  },

  setDepSearchCriteria({ commit }, val) {
    commit(MutationTypes.SET_DEP_FILTER_STRING, val);
  },

  setScriptSearchCriteria({ commit }, val) {
    commit(MutationTypes.SET_SCRIPT_FILTER_STRING, val);
  },

  setDepSearchTypes({ commit }, val) {
    commit(MutationTypes.SET_DEP_SEARCH_TYPE, val);
  },

  setDepNeedUpdateFilter({ commit }, val) {
    commit(MutationTypes.SET_DEP_NEED_UPDATE_FILTER, val);
  },

  executeScript({ commit, state }, name) {
    async(function*() {
      commit(MutationTypes.START_EXECUTE_SCRIPT, name);

      try {
        shell.cd(state.data.locationPath);
        yield execCommandAsync(`npm run ${name}`);
        showSuccess("Executing selected script successfully!");
      } catch (err) {
        console.log(err);
        showError("Error occured when executing selected script!");
      } finally {
        commit(MutationTypes.DONE_EXECUTE_SCRIPT, name);
      }
    });
  },

  removeScript({ commit, state }, name) {
    async(function*() {
      commit(MutationTypes.START_REMOVE_SCRIPT, name);
      try {
        let pkgJsonPath = path.resolve(state.data.locationPath, "package.json");
        let pkgJson = yield readJsonFileAsync(pkgJsonPath);
        if (name in pkgJson.scripts) {
          delete pkgJson.scripts[name];
        }

        yield writeJsonFileAsync(pkgJsonPath, pkgJson);
        commit(MutationTypes.REMOVE_SCRIPT_SUCCEED, name);
      } catch (err) {
        console.error(err);
        showError("Error occured when removing package script!");
      } finally {
        commit(MutationTypes.DONE_REMOVE_SCRIPT);
      }
    });
  },

  installDependency({ commit, state, dispatch }, name) {
    async(function*() {
      commit(MutationTypes.FETCH_DATA);
      try {
        yield installPackageAsync(state.data.locationPath, name, "--save");
        showSuccess(`Install ${name} successfully!`);
        dispatch(
          `${PackageModule.Local}/reloadSelectedFolderPackage`,
          undefined,
          { root: true }
        );
      } catch (err) {
        console.error(err);
        showError(`Fail to install ${name}!`);
      } finally {
        commit(MutationTypes.DONE_FETCHING_DATA);
      }
    });
  }
};

export default actions;
