import { vueDeleteObjectProperty } from "utils";

export const MutationTypes = {
  FETCH_DATA: "LOCAL_SELECTED_FETCH_DATA",
  DONE_FETCHING_DATA: "LOCAL_SELECTED_DONE_FETCHING_DATA",
  SET_DATA: "LOCAL_SELECTED_SET_DATA",

  UNINSTALL_PACKAGE_SUCCEED: "LOCAL_SELECTED_UNINSTALL_DEP_PACKAGE_SUCCEED",
  START_UNINSTALL_PACKAGE: "LOCAL_SELECTED_START_UNINSTALL_DEP_PACKAGE",
  DONE_UNINSTALL_PACKAGE: "LOCAL_SELECTED_DONE_UNINSTALL_DEP_PACKAGE",

  EXECUTE_SCRIPT_SUCCEED: "LOCAL_SELECTED_EXECUTE_SCRIPT_SUCCEED",
  START_EXECUTE_SCRIPT: "LOCAL_SELECTED_START_EXECUTE_SCRIPT",
  DONE_EXECUTE_SCRIPT: "LOCAL_SELECTED_DONE_EXECUTE_SCRIPT",

  SET_DEP_FILTER_STRING: "LOCAL_SELECTED_SET_DEP_FILTER_STRING",
  SET_DEP_SEARCH_TYPE: "LOCAL_SELECTED_SET_DEPENDENCY_SEARCH_TYPE",
  SET_DEP_NEED_UPDATE_FILTER: "LOCAL_SELECTED_SET_DEP_NEED_UPDATE_FILTER",

  SET_SCRIPT_FILTER_STRING: "LOCAL_SELECTED_SET_SCRIPT_FILTER_STRING",

  START_REMOVE_SCRIPT: "LOCAL_SELECTED_START_REMOVE_SCRIPT",
  DONE_REMOVE_SCRIPT: "LOCAL_SELECTED_DONE_REMOVE_SCRIPT",
  REMOVE_SCRIPT_SUCCEED: "LOCAL_SELECTED_REMOVE_SCRIPT_SUCCEED"
};

const mutations = {
  [MutationTypes.FETCH_DATA]: state => {
    state.loading = true;
  },

  [MutationTypes.DONE_FETCHING_DATA]: state => {
    state.loading = false;
  },

  [MutationTypes.SET_DATA]: (state, data) => {
    state.data = data;
  },

  [MutationTypes.UNINSTALL_PACKAGE_SUCCEED]: (state, { pkgName, type }) => {
    vueDeleteObjectProperty(state.data[type], pkgName);
    state.uninstallingDep = "";
  },

  [MutationTypes.START_UNINSTALL_PACKAGE]: (state, name) => {
    state.uninstallingDep = name;
  },

  [MutationTypes.DONE_UNINSTALL_PACKAGE]: state => {
    state.uninstallingDep = "";
  },

  [MutationTypes.EXECUTE_SCRIPT_SUCCEED]: state => {
    state.executingScript = "";
  },

  [MutationTypes.START_EXECUTE_SCRIPT]: (state, name) => {
    state.executingScript = name;
  },

  [MutationTypes.DONE_EXECUTE_SCRIPT]: state => {
    state.executingScript = "";
  },

  [MutationTypes.SET_DEP_FILTER_STRING]: (state, val) => {
    state.depSearchCriteria.strFilter = val;
  },

  [MutationTypes.SET_DEP_SEARCH_TYPE]: (state, val) => {
    state.depSearchCriteria.types = val;
  },

  [MutationTypes.SET_DEP_NEED_UPDATE_FILTER]: (state, val) => {
    state.depSearchCriteria.needUpdate = val;
  },

  [MutationTypes.SET_SCRIPT_FILTER_STRING]: (state, val) => {
    state.scriptSearchCriteria.strFilter = val;
  },

  [MutationTypes.REMOVE_SCRIPT_SUCCEED]: (state, val) => {
    vueDeleteObjectProperty(state.data.scripts, val);
  },

  [MutationTypes.START_REMOVE_SCRIPT]: (state, name) => {
    state.removingScript = name;
  },

  [MutationTypes.DONE_REMOVE_SCRIPT]: state => {
    state.removingScript = "";
  }
};

export default mutations;
