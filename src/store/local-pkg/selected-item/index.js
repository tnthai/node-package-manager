import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import { DependencyTypes } from "../../../constants";

const store = {
  namespaced: true,
  state: {
    data: {},
    uninstallingDep: "",
    executingScript: "",
    removingScript: "",
    depSearchCriteria: {
      strFilter: "",
      types: [
        DependencyTypes.Normal,
        DependencyTypes.Dev,
        DependencyTypes.Optional
      ],
      needUpdate: false
    },
    scriptSearchCriteria: {
      strFilter: ""
    },
    loading: false
  },
  getters,
  actions,
  mutations
};

export default store;
