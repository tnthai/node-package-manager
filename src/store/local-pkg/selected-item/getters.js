import { DependencyTypes } from "../../../constants";
import { uniqBy, isEmpty } from "lodash";

const getters = {
  isAppLoading: state => state.loading,
  item: state => state.data,
  depFilterNeedUpdate: state => state.depSearchCriteria.needUpdate,
  depFilterString: state => state.depSearchCriteria.strFilter,
  scriptFilterString: state => state.scriptSearchCriteria.strFilter,
  depFilterTypes: state => state.depSearchCriteria.types,
  depFilterTypeItems: state => [
    {
      name: "default",
      id: DependencyTypes.Normal
    },
    {
      name: "dev",
      id: DependencyTypes.Dev
    },
    {
      name: "optional",
      id: DependencyTypes.Optional
    }
  ],
  displayDependencies: state => {
    let selectedPkg = state.data;
    if (
      !selectedPkg ||
      !selectedPkg.dependencies ||
      !selectedPkg.devDependencies
    ) {
      return [];
    }

    const { dependencies, devDependencies } = selectedPkg;

    return filterDependencies(
      Object.keys(dependencies)
        .map(k => ({
          name: k,
          type: DependencyTypes.Normal
        }))
        .concat(
          Object.keys(devDependencies).map(k => ({
            name: k,
            type: DependencyTypes.Dev
          }))
        ),
      state.depSearchCriteria,
      state.data
    );
  },
  displayScripts: state => {
    let selectedPkg = state.data;
    if (!selectedPkg || !selectedPkg.scripts) {
      return [];
    }

    const { scripts } = selectedPkg;
    return filterScripts(
      Object.keys(scripts).map(k => ({
        name: k,
        value: scripts[k]
      })),
      state.scriptSearchCriteria
    );
  },
  uninstallingDep: state => state.uninstallingDep,
  executingScript: state => state.executingScript,
  removingScript: state => state.removingScript,
  isDepUpgradeDisabled: state => name =>
    !isEmpty(state.data.packageStatus) && !(name in state.data.packageStatus),
  depsTableHeaders: state => [
    {
      text: "Package",
      value: "package",
      sortable: true
    },
    {
      text: "",
      value: "action",
      align: "center",
      sortable: false
    }
  ],
  scriptsTableHeaders: state => [
    {
      text: "Name",
      value: "name",
      sortable: true
    },
    {
      text: "Value",
      value: "value",
      sortable: false
    },
    {
      text: "",
      value: "action",
      align: "center",
      sortable: false
    }
  ]
};

export default getters;

function filterDependencies(items, { strFilter, types, needUpdate }, data) {
  return uniqBy(
    items.filter(
      i =>
        i.name.indexOf(strFilter) > -1 &&
        types.indexOf(i.type) > -1 &&
        (!needUpdate || !data.packageStatus || i.name in data.packageStatus)
    ),
    "name"
  );
}

function filterScripts(items, { strFilter }) {
  let lowerCaseFilter = strFilter.toLowerCase();
  return uniqBy(
    items.filter(
      i =>
        i.name.toLowerCase().indexOf(lowerCaseFilter) > -1 ||
        i.value.toLowerCase().indexOf(lowerCaseFilter) > -1
    ),
    "name"
  );
}
