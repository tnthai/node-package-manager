export const MutationTypes = {
  FETCH_DATA: "GLOBAL_FETCH_DATA",
  DONE_FETCHING_DATA: "GLOBAL_DONE_FETCHING_DATA",

  SET_FILTER: "GLOBAL_SET_FILTER",
  FETCH_PACKAGES_SUCCEED: "GLOBAL_FETCH_PACKAGES_SUCCEED",

  START_UNINSTALL_PACKAGE: "GLOBAL_START_UNINSTALL_PACKAGE",
  UNINSTALL_PACKAGE_SUCCEED: "GLOBAL_UNINSTALL_PACKAGE_SUCCEED",

  START_UPGRADE_PACKAGE: "GLOBAL_START_UPGRADE_PACKAGE",
  UPGRADE_PACKAGE_SUCCEED: "GLOBAL_UPGRADE_PACKAGE_SUCCEED",

  INSTALL_PACKAGE_SUCCEED: "GLOBAL_INSTALL_PACKAGE_SUCCEED"
};

const mutations = {
  [MutationTypes.FETCH_PACKAGES_SUCCEED]: (state, packageResult) => {
    state.items = packageResult.items;
    state.location = packageResult.location;
  },

  [MutationTypes.UNINSTALL_PACKAGE_SUCCEED]: (state, name) => {
    let globalItems = state.items;
    let removedItemIndex = globalItems.findIndex(item => item.name === name);

    if (removedItemIndex >= 0) {
      globalItems.splice(removedItemIndex, 1);
    }
  },

  [MutationTypes.START_UNINSTALL_PACKAGE]: (state, name) => {
    state.items.forEach(pkg => {
      if (pkg.name === name) {
        pkg.uninstalling = true;
      } else {
        return;
      }
    });
  },

  [MutationTypes.START_UPGRADE_PACKAGE]: (state, name) => {
    state.items.forEach(pkg => {
      if (pkg.name === name) {
        pkg.upgrading = true;
      } else {
        return;
      }
    });
  },

  [MutationTypes.UPGRADE_PACKAGE_SUCCEED]: (state, upgradedPkg) => {
    state.items.forEach(pkg => {
      if (pkg.name === upgradedPkg.name) {
        pkg.version = upgradedPkg.version;
        pkg.description = upgradedPkg.description;
        pkg.upgrading = false;
      } else {
        return;
      }
    });
  },

  [MutationTypes.INSTALL_PACKAGE_SUCCEED]: (state, packageItem) => {
    let globalItems = state.items;
    if (!globalItems.some(item => item.name === packageItem.name)) {
      globalItems.push(packageItem);
    }
  },

  [MutationTypes.SET_FILTER]: (state, val) => {
    state.searchFilterString = val;
  },

  [MutationTypes.FETCH_DATA]: (state, val) => {
    state.loading = true;
  },

  [MutationTypes.DONE_FETCHING_DATA]: (state, val) => {
    state.loading = false;
  },

  [MutationTypes.START_INSTALL_GLOBAL_PACKAGE]: (state, name) => {
    // TODO
  }
};

export default mutations;
