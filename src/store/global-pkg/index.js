import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
import configStore from "./configuration";

const store = {
  namespaced: true,
  state: {
    items: [],
    location: "",
    loading: false,
    searchFilterString: ""
  },
  modules: {
    configuration: configStore
  },
  getters,
  actions,
  mutations
};

export default store;
