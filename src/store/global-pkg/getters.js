import { isString } from "lodash";

const getters = {
  searchFilterString: state => state.searchFilterString,
  displayItems: state => {
    if (!state.searchFilterString) {
      return state.items;
    }

    const searchTerm = state.searchFilterString.toLowerCase();
    const displayFields = state.configuration.selectedDisplayFields;
    return state.items.filter(item =>
      displayFields.some(
        field =>
          isString(item[field]) &&
          item[field].toLowerCase().indexOf(searchTerm) > -1
      )
    );
  },
  loading: state => state.loading
};

export default getters;
