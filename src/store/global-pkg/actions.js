import { MutationTypes } from "./mutations";
import {
  execCommandAsync,
  readJsonFileAsync,
  async,
  checkPackagesAsync,
  untilFileExist
} from "utils/promises";
import shell from "shelljs";
import path from "path";
import { showError } from "utils/ui-notification";
import PackageWatcher, { ChangeMode } from "utils/packageWatcher";

const actions = {
  setPackageFilter({ commit }, val) {
    commit(MutationTypes.SET_FILTER, val);
  },

  fetchPackages({ commit }) {
    async(function*() {
      commit(MutationTypes.FETCH_DATA);
      try {
        let stdoutLocation = yield execCommandAsync("npm root -g");
        let rootLocation = stdoutLocation.trim();

        let packageJsonResults$ = shell
          .ls(rootLocation)
          .map(moduleName =>
            readJsonFileAsync(
              path.resolve(rootLocation, moduleName, "package.json"),
              { isNpm: true }
            )
          );

        let pkgsWithStatus = yield checkPackagesAsync();
        const pkgNeedUpdate = name => {
          let sttObj = pkgsWithStatus[name];
          if (!sttObj) {
            return false;
          }

          return (
            sttObj.current !== sttObj.wanted && sttObj.current !== sttObj.latest
          );
        };

        let results = yield Promise.all(packageJsonResults$);
        commit(MutationTypes.FETCH_PACKAGES_SUCCEED, {
          location: rootLocation,
          items: results.map(item =>
            Object.assign({}, item, {
              upgrading: false,
              uninstalling: false,
              needUpdate: pkgNeedUpdate(item.name)
            })
          )
        });

        PackageWatcher.listen(rootLocation, (err, data) => {
          if (!err) {
            const { mode, name, path: pkgPath } = data;
            if (mode === ChangeMode.Remove) {
              commit(MutationTypes.UNINSTALL_PACKAGE_SUCCEED, name);
            } else if (mode === ChangeMode.Add) {
              const pkgJsonLocation = path.resolve(pkgPath, "package.json");
              untilFileExist(pkgJsonLocation).then(() => {
                readJsonFileAsync(pkgJsonLocation, {
                  isNpm: true
                }).then(item => {
                  commit(
                    MutationTypes.INSTALL_PACKAGE_SUCCEED,
                    Object.assign({}, item, {
                      upgrading: false,
                      uninstalling: false,
                      needUpdate: false
                    })
                  );
                });
              });
            }
          }
        });

        commit(MutationTypes.DONE_FETCHING_DATA);
      } catch (err) {
        console.error(err);
        showError("Something wrong occured when attempting to fetch packages!");
        commit(MutationTypes.DONE_FETCHING_DATA);
      }
    });
  },

  upgradePackage({ state, commit }, name) {
    async(function*() {
      commit(MutationTypes.START_UPGRADE_PACKAGE, name);
      try {
        yield execCommandAsync(`npm install -g ${name}@latest`);
        let moduleData = yield readJsonFileAsync(
          path.resolve(state.location, name, "package.json"),
          {
            isNpm: true
          }
        );

        let pkg = state.items.find(item => item.name === name);
        commit(
          MutationTypes.UPGRADE_PACKAGE_SUCCEED,
          Object.assign({}, pkg, {
            version: moduleData.version,
            upgrading: false
          })
        );
      } catch (err) {
        console.error("Error when upgrading package " + name, err);
      }
    });
  },

  uninstallPackage({ commit }, name) {
    async(function*() {
      commit(MutationTypes.START_UNINSTALL_PACKAGE, name);
      try {
        yield execCommandAsync(`npm uninstall -g ${name}`);
        commit(MutationTypes.UNINSTALL_PACKAGE_SUCCEED, name);
      } catch (err) {
        console.error("Error when uninstalling package " + name, err);
      }
    });
  }
};

export default actions;
