import { MutationTypes } from "./mutations";
import { execCommandAsync, async } from "utils/promises";
import { PackageModule } from "../../../constants";
import { readAsync } from "fs-jetpack";
import ini from "ini";
import { isString, isNumber, isBoolean, capitalize } from "lodash";

const actions = {
  fetchNpmConfig({ commit, state, rootState }) {
    async(function*() {
      commit(MutationTypes.START_FETCHING_CONFIG);
      let npmrcLocation = rootState[PackageModule.Global].location.replace(
        "node_modules",
        "etc\\npmrc"
      );
      let rawCfg = yield execCommandAsync("npm config list -g --json");
      let userCfg = yield readAsync(npmrcLocation);
      let finalCfg = Object.assign(
        {},
        JSON.parse(rawCfg),
        userCfg ? ini.parse(userCfg) : {}
      );

      commit(
        MutationTypes.FETCH_CONFIGURATION_SUCCEED,
        Object.keys(finalCfg).map(key => {
          let type = "string";
          let value = finalCfg[key];
          isString(value) && (type = "string");
          isNumber(value) && (type = "number");
          isBoolean(value) && (type = "bool");

          return {
            key,
            label: capitalize(key),
            type,
            value,
            dirtyVal: value,
            saving: false
          };
        })
      );
    });
  },

  saveNpmConfig({ commit, state }) {
    async(function*() {
      commit(MutationTypes.START_FETCHING_CONFIG);

      let cmds$ = state.npm
        .filter(cfg => cfg.dirtyVal != cfg.value)
        .map(cfg =>
          execCommandAsync(`npm config set ${cfg.key} ${cfg.dirtyVal} -g`)
        );

      yield Promise.all(cmds$);
      commit(MutationTypes.FETCH_CONFIGURATION_SUCCEED, []);
      commit(MutationTypes.TOGGLE_CONFIGURATION_DIALOG);
    });
  },

  setDisplayFields({ commit }, val) {
    commit(MutationTypes.SET_DISPLAY_FIELDS, val);
  },

  toggleConfigDialog({ commit }) {
    commit(MutationTypes.TOGGLE_CONFIGURATION_DIALOG);
  },

  updateConfigPageIndex({ commit }, val) {
    commit(MutationTypes.UPDATE_CONFIG_PAGEINDEX, val);
  },

  updateConfigItem({ commit }, payload) {
    commit(MutationTypes.UPDATE_CONFIG_ITEM, payload);
  }
};

export default actions;
