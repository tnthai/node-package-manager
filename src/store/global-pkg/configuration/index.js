import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";

const store = {
  namespaced: true,
  state: {
    loading: false,
    visible: false,
    availableDisplayFields: [
      "name",
      "description",
      "version",
      "license",
      "engines",
      "keywords",
      "action"
    ],
    selectedDisplayFields: ["name", "description", "version", "action"],
    npm: [],
    npmConfigPageIndex: 1
  },
  getters,
  actions,
  mutations
};

export default store;
