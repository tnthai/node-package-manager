export const MutationTypes = {
  SET_DISPLAY_FIELDS: "GLOBALCONFIG_SET_DISPLAY_FIELDS",
  TOGGLE_CONFIGURATION_DIALOG: "GLOBALCONFIG_TOGGLE_CONFIGURATION_DIALOG",
  FETCH_CONFIGURATION_SUCCEED: "GLOBALCONFIG_FETCH_CONFIGURATION_SUCCEED",
  START_FETCHING_CONFIG: "GLOBALCONFIG_START_FETCHING_CONFIG",
  UPDATE_CONFIG_PAGEINDEX: "GLOBALCONFIG_UPDATE_CONFIG_PAGEINDEX",
  UPDATE_CONFIG_ITEM: "GLOBALCONFIG_UPDATE_CONFIG_ITEM"
};

const mutations = {
  [MutationTypes.TOGGLE_CONFIGURATION_DIALOG]: (state, val) => {
    state.visible = !state.visible;
  },
  [MutationTypes.FETCH_CONFIGURATION_SUCCEED]: (state, configs) => {
    state.npm = configs;
    state.loading = false;
  },

  [MutationTypes.START_FETCHING_CONFIG]: state => {
    state.loading = true;
    state.npm = [];
  },

  [MutationTypes.UPDATE_CONFIG_PAGEINDEX]: (state, val) => {
    state.npmConfigPageIndex = val;
  },

  [MutationTypes.UPDATE_CONFIG_ITEM]: (state, { key, newVal }) => {
    let configIndex = state.npm.findIndex(item => item.key === key);

    if (configIndex >= 0) {
      state.npm[configIndex].dirtyVal = newVal;
    }
  },

  [MutationTypes.SET_DISPLAY_FIELDS]: (state, val) => {
    state.selectedDisplayFields = val;
  }
};

export default mutations;
