import { capitalize } from "lodash";
import { ActionVisibleMode } from "controls/DataTable/data-table";

const getters = {
  data: state => state,
  npmConfigTotalPages: state => Math.ceil(state.npm.length / 10),
  npmConfigItems: state => {
    let pageIndex = state.npmConfigPageIndex;
    return state.npm.slice((pageIndex - 1) * 10, 10 * pageIndex);
  },
  selectedDisplayFields: state => state.selectedDisplayFields,
  tablePkgHeaders: state => {
    let contentCols = state.selectedDisplayFields.map(field => ({
      text: capitalize(field),
      value: field,
      align: "left",
      sortable: true
    }));

    contentCols.push({
      text: "",
      value: "action",
      align: "center",
      sortable: false
    });

    return contentCols;
  },
  tableColumns: state => {
    let allColumns = [
      {
        columnName: "name",
        headerName: "Name",
        columnFlex: 1
      },
      {
        columnName: "description",
        headerName: "Description",
        columnFlex: 1
      },
      {
        columnName: "version",
        headerName: "Version",
        cellClassName: "flex-fixed-150",
        headerClassName: "flex-fixed-150"
      },
      {
        columnName: "license",
        headerName: "License",
        cellClassName: "flex-fixed-150",
        headerClassName: "flex-fixed-150"
      },
      {
        columnName: "engines",
        headerName: "Engines",
        cellClassName: "flex-fixed-150",
        headerClassName: "flex-fixed-150"
      },
      {
        columnName: "keywords",
        headerName: "Keywords",
        columnFlex: 1
      },
      {
        columnName: "action",
        headerName: "",
        cellClassName: "flex-fixed-200",
        headerClassName: "flex-fixed-200",
        visibleMode: ActionVisibleMode.OnTouch
      }
    ];

    return allColumns.filter(
      col => state.selectedDisplayFields.indexOf(col.columnName) > -1
    );
  }
};

export default getters;
