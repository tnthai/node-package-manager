export const MutationTypes = {
  FETCH_DATA: "MARKET_FETCH_DATA",
  DONE_FETCHING_DATA: "MARKET_DONE_FETCHING_DATA",
  SEARCH_PACKAGES_SUCCEED: "MARKET_SEARCH_PACKAGES_SUCCEED",
  SET_DISPLAY_FIELDS: "MARKET_SET_DISPLAY_FIELDS",
  SET_FILTER: "MARKET_SET_PACKAGE_FILTER",
  START_INSTALL_PACKAGE: "MARKET_START_INSTALL_PACKAGE",
  DONE_INSTALL_PACKAGE: "MARKET_DONE_INSTALL_PACKAGE",
  TOGGLE_PROJECT_SELECTION_DIALOG: "MARKET_TOGGLE_PROJECT_SELECTION_DIALOG"
};

const mutations = {
  [MutationTypes.SEARCH_PACKAGES_SUCCEED]: (state, packageResult) => {
    state.searchResult.items = packageResult.items;
    state.searchResult.pageIndex = packageResult.pageIndex;
    state.searchResult.totalCount = packageResult.totalCount;
  },

  [MutationTypes.SET_DISPLAY_FIELDS]: (state, val) => {
    state.searchFilter.selectedDisplayFields = val;
  },

  [MutationTypes.SET_FILTER]: (state, val) => {
    state.searchFilter.query = val;
  },

  [MutationTypes.TOGGLE_PROJECT_SELECTION_DIALOG]: state => {
    state.prjSelectionDialogVisible = !state.prjSelectionDialogVisible;
  },

  [MutationTypes.FETCH_DATA]: state => {
    state.loading = true;
  },

  [MutationTypes.DONE_FETCHING_DATA]: state => {
    state.loading = false;
  },

  [MutationTypes.START_INSTALL_PACKAGE]: (state, name) => {
    state.searchResult.items.forEach(pkg => {
      if (pkg.name !== name) {
        return;
      }

      pkg.installing = true;
    });
  },

  [MutationTypes.DONE_INSTALL_PACKAGE]: (state, name) => {
    state.searchResult.items.forEach(pkg => {
      if (pkg.name !== name) {
        return;
      }

      pkg.installing = false;
    });
  }
};

export default mutations;
