import { MutationTypes } from "./mutations";
import {
  execCommandAsync,
  async,
  installPackageAsync,
  requestAsync
} from "utils/promises";
import { appConfig } from "config";
import { assert } from "chai";
import { isEmpty } from "lodash";
import {
  showSuccess,
  showWarning,
  showError
} from "utils/ui-notification";
import { PackageModule } from "../../constants";
import shell from "shelljs";

const actions = {
  toggleProjectSelectionDialog({ commit }) {
    commit(MutationTypes.TOGGLE_PROJECT_SELECTION_DIALOG);
  },

  setFilter({ commit }, val) {
    commit(MutationTypes.SET_FILTER, val);
  },

  setDisplayFields({ commit }, val) {
    commit(MutationTypes.SET_DISPLAY_FIELDS, val);
  },

  searchPackageOnline({ commit, state }, pageIndex) {
    async(function*() {
      commit(MutationTypes.FETCH_DATA);
      let { query, selectedDisplayFields } = state.searchFilter;
      let { totalCount, pageSize } = state.searchResult;

      let searchFrom =
        ((pageIndex == undefined ? 1 : pageIndex) - 1) * pageSize;

      let sizeToSearch =
        totalCount === 0
          ? pageSize
          : totalCount - searchFrom < pageSize
            ? totalCount - searchFrom
            : pageSize;

      try {
        const url = `${
          appConfig.RegistrySearchUrl
        }?text=${query}&from=${searchFrom}&size=${sizeToSearch}`;

        let { body } = yield requestAsync(url, {
          json: true
        });

        const getPkgAuthor = authorObj => (authorObj ? authorObj.name : "");
        const getPkgObject = item =>
          selectedDisplayFields.reduce(
            (acc, cur) =>
              Object.assign(acc, {
                [cur]:
                  cur === "author"
                    ? getPkgAuthor(item.package[cur])
                    : item.package[cur]
              }),
            {
              installing: false
            }
          );

        commit(MutationTypes.SEARCH_PACKAGES_SUCCEED, {
          totalCount: body.total,
          pageIndex,
          pageSize,
          items: body.objects.map(pkg => getPkgObject(pkg))
        });

        commit(MutationTypes.DONE_FETCHING_DATA);
      } catch (err) {
        showError("Something wrong when searching packages!");
        console.error(err);
      }
    });
  },

  installPackage({ commit, dispatch }, options) {
    assert.property(options, "name", "Lack package name when installing!");
    if (options.isGlobal == undefined) {
      options.isGlobal = true;
    }

    if (options.isGlobal) {
      installPackageGlobally(commit, options.name);
    } else if (Array.isArray(options.selectedProjects)) {
      installPackageToLocalPackages(
        commit,
        options.name,
        options.selectedProjects
      );
    } else {
      commit(MutationTypes.TOGGLE_PROJECT_SELECTION_DIALOG);
      dispatch(`${PackageModule.Local}/fetchPackageFolders`, null, {
        root: true
      });
    }
  }
};

function installPackageGlobally(commit, name) {
  async(function*() {
    commit(MutationTypes.START_INSTALL_PACKAGE, name);
    let result = yield execCommandAsync(`npm list -g ${name} --depth=0 --json`);
    if (!isEmpty(JSON.parse(result.trim()))) {
      showWarning("This package is already globally installed!");
      commit(MutationTypes.DONE_INSTALL_PACKAGE, name);
    } else {
      yield execCommandAsync(`npm install -g ${name}`);
      commit(MutationTypes.DONE_INSTALL_PACKAGE, name);
      showSuccess("Package installed successfully!");
    }
  });
}

function installPackageToLocalPackages(commit, pkgName, pkgs) {
  async(function*() {
    commit(MutationTypes.START_INSTALL_PACKAGE, pkgName);
    let prjCount = pkgs.length;
    for (let proj of pkgs) {
      shell.cd(proj.path);
      try {
        yield installPackageAsync(proj.path, pkgName, proj.installOptions);
        prjCount--;
        if (prjCount === 0) {
          commit(MutationTypes.DONE_INSTALL_PACKAGE, pkgName);
        }
        showSuccess(`Install ${pkgName} to ${proj.name} successfully!`);
      } catch (err) {
        console.error(err);
        showError(`Fail to install ${pkgName} to ${proj.name}!`);
      }
    }
  });
}

export default actions;
