import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const store = {
  namespaced: true,
  state: {
    searchResult: {
      pageIndex: 1,
      pageSize: 10,
      totalCount: 0,
      items: []
    },

    searchFilter: {
      query: "",
      availableDisplayFields: [
        "name",
        "author",
        "description",
        "version",
        "rating",
        "keywords",
        "maintainers",
        "readme"
      ],
      selectedDisplayFields: ["name", "author", "description"]
    },
    loading: false,
    prjSelectionDialogVisible: false
  },
  getters,
  actions,
  mutations
};

export default store;
