import { capitalize } from "lodash";

const getters = {
  resultTotalPages: state =>
    Math.floor(state.searchResult.totalCount / state.searchResult.pageSize) + 1,
  searchedPackageResult: state => state.searchResult,
  searchedPackageFilter: state => state.searchFilter,
  isLoading: state => state.loading,
  isPrjSelectionDialogVisible: state => state.prjSelectionDialogVisible,
  tablePkgHeaders: state => {
    let contentCols = state.searchFilter.selectedDisplayFields.map(field => ({
      text: capitalize(field),
      value: field,
      sortable: false
    }));

    contentCols.push({
      text: "",
      value: "action",
      align: "center",
      sortable: false
    });

    return contentCols;
  }
};

export default getters;
