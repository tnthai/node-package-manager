import Vue from "vue";
import Vuex from "vuex";
import uiStore from "./ui";
import gbStore from "./global-pkg";
import npmStore from "./npm-pkg";
import localStore from "./local-pkg";
import pkgInfoStore from "./pkg-info";
import { PackageModule } from "../constants";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    [PackageModule.Ui]: uiStore,
    [PackageModule.Global]: gbStore,
    [PackageModule.NpmMarket]: npmStore,
    [PackageModule.Local]: localStore,
    [PackageModule.PackageInfo]: pkgInfoStore
  },
  getters: {
    appRoute: state => state.route
  },
  strict: process.env.NODE_ENV === "development"
});

export default store;
