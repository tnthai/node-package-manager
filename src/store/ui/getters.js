import { isString } from "lodash";
import objectPath from "object-path";

const getters = {
  isAppLoading: state => state.loading,
  isDrawerVisible: state => state.drawerVisible,
  drawerMenuItems: state => state.drawerMenuItems,
  appPaths: state => state.appPaths,
  windowResizing: state => state.windowResizing,
  toolbarMenus: state => (type, context) => {
    if (!isString(type)) {
      type = type.toString();
    }
    if (!(type in state.toolbarMenus)) {
      return [];
    }

    return state.toolbarMenus[type].map(menu =>
      Object.keys(menu).reduce((acc, cur) => {
        let defaultRes = Object.assign(acc, {
          [cur]: menu[cur]
        });

        if (isString(menu[cur])) {
          return defaultRes;
        }

        switch (menu[cur].type) {
          case "dynProp":
            return Object.assign(acc, {
              [cur]: objectPath.get(context, menu[cur].path)
            });
          case "dynFunc":
            return Object.assign(acc, {
              [cur]: () => objectPath.get(context, menu[cur].path)()
            });
          default:
            return defaultRes;
        }
      }, {})
    );
  }
};

export default getters;
