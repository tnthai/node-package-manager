export const MutationTypes = {
  LOAD_APP: "UI_LOAD_APP",
  DONE_LOADING_APP: "UI_DONE_LOADING_APP",
  TOGGLE_APP_DRAWER: "UI_TOGGLE_APP_DRAWER",
  SET_APP_USERDATA_PATH: "UI_SET_APP_USERDATA_PATH",
  RESIZE_WINDOW: "UI_RESIZE_WINDOW",
  DONE_RESIZE_WINDOW: "UI_DONE_RESIZE_WINDOW"
};

const mutations = {
  [MutationTypes.LOAD_APP]: state => {
    state.loading = true;
  },

  [MutationTypes.DONE_LOADING_APP]: state => {
    state.loading = false;
  },

  [MutationTypes.TOGGLE_APP_DRAWER]: state => {
    state.drawerVisible = !state.drawerVisible;
  },

  [MutationTypes.SET_APP_USERDATA_PATH]: (state, userData) => {
    state.appPaths = Object.assign({}, state.appPaths, {
      userData
    });
  },

  [MutationTypes.RESIZE_WINDOW]: state => {
    state.windowResizing = true;
  },

  [MutationTypes.DONE_RESIZE_WINDOW]: state => {
    state.windowResizing = false;
  }
};

export default mutations;
