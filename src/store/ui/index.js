import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import { drawerMenus, toolbarMenus } from "./menus";

const store = {
  namespaced: true,
  state: {
    loading: false,
    drawerVisible: true,
    drawerMenuItems: drawerMenus,
    appPaths: {},
    toolbarMenus,
    windowResizing: false
  },
  getters,
  actions,
  mutations
};

export default store;
