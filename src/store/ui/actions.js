import { MutationTypes } from "./mutations";
import { getUserDataPathAsync, async } from "utils/promises";

const actions = {
  startLoadingApp({ commit }) {
    commit(MutationTypes.LOAD_APP);
  },

  toggleAppDrawer({ commit }) {
    commit(MutationTypes.TOGGLE_APP_DRAWER);
  },

  doneFetchingData({ commit }) {
    commit(MutationTypes.DONE_LOADING_APP);
  },

  getUserDataPath({ commit }) {
    async(function*() {
      commit(MutationTypes.SET_APP_USERDATA_PATH, yield getUserDataPathAsync());
    });
  },

  initResizeEvents({ commit }) {
    let resizeTimer;
    window.onresize = e => {
      commit(MutationTypes.RESIZE_WINDOW);
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(() => {
        commit(MutationTypes.DONE_RESIZE_WINDOW);
      }, 250);
    };
  }
};

export default actions;
