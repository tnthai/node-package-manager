import { PackageModule } from "../../constants";

export const drawerMenus = [
  {
    icon: "dashboard",
    title: "Dashboard",
    to: "/",
    subItems: []
  },
  {
    icon: "file_download",
    title: "Installed Packages",
    subItems: [
      {
        icon: "computer",
        title: "Global",
        to: "/packages/global"
      },
      {
        icon: "folder",
        title: "Local",
        to: "/packages/local"
      }
    ]
  },
  {
    icon: "shopping_cart",
    title: "NPM Market",
    to: "/market",
    subItems: []
  }
];

export const toolbarMenus = {
  [PackageModule.Global]: [
    {
      name: "refresh",
      icon: "refresh",
      title: "Refresh",
      onClick: { type: "dynFunc", path: ["refreshGlobalPackages"] }
    },
    {
      name: "settings",
      icon: "settings",
      title: "View global settings",
      disabled: {
        type: "dynProp",
        path: ["globalPackageData", "loading"]
      },
      onClick: { type: "dynFunc", path: ["onSettingsClick"] }
    }
  ],
  [PackageModule.NpmMarket]: [
    {
      name: "settings",
      icon: "settings",
      title: "View page settings",
      onClick: { type: "dynFunc", path: ["onSettingsClick"] }
    }
  ],
  [PackageModule.Local]: [
    {
      name: "add",
      icon: "add",
      title: "Add new package folder",
      onClick: { type: "dynFunc", path: ["addLocalPackage"] }
    },
    {
      name: "settings",
      icon: "settings",
      title: "View page settings",
      onClick: e => {}
    }
  ]
};
