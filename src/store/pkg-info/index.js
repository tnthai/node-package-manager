import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const store = {
  namespaced: true,
  state: {
    visible: false,
    loading: true,
    data: {}
  },
  getters,
  actions,
  mutations
};

export default store;
