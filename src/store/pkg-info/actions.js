import { MutationTypes } from "./mutations";
import { getPackageDetailAsync, async } from "utils/promises";

const actions = {
  fetchPackageInfo({ commit, state }, name) {
    async(function*() {
      commit(MutationTypes.FETCH_SUCCEED, yield getPackageDetailAsync(name));
    });
  },

  toggleDialog({ commit }, forceClose) {
    commit(MutationTypes.TOGGLE_DIALOG, forceClose);
  }
};

export default actions;
