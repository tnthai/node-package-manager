export const MutationTypes = {
  FETCH_SUCCEED: "PACKAGE_INFO_FETCH_SUCCEED",
  TOGGLE_DIALOG: "PACKAGE_INFO_TOGGLE_DIALOG"
};

const mutations = {
  [MutationTypes.TOGGLE_DIALOG]: (state, forceClose) => {
    state.visible = forceClose ? false : !state.visible;

    if (!state.visible) {
      state.loading = true;
      state.data = {};
    }
  },

  [MutationTypes.FETCH_SUCCEED]: (state, val) => {
    state.data = val;
    state.loading = false;
  }
};

export default mutations;
