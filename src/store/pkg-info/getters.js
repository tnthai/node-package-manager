import { Converter } from "showdown";
import { capitalize, isArray, isString } from "lodash";
import { getRandomColor } from "utils";

const getters = {
  visible: state => state.visible,
  loading: state => state.loading,
  data: state => state.data,
  getReadmeAsHtml: state => {
    let content = state.data.readme;
    if (!content) {
      return "";
    }

    try {
      let converter = new Converter();
      return converter.makeHtml(content);
    } catch (err) {
      console.warn("Error when parsing readme to html ", err);
      return content;
    }
  },
  evenInfos: state =>
    Object.keys(state.data)
      .filter(
        (key, index) =>
          key !== "readme" && key !== "readmeFilename" && index % 2 === 0
      )
      .map(key => getItem(key, state.data[key])),

  oddInfos: state =>
    Object.keys(state.data)
      .filter(
        (key, index) =>
          key !== "readme" && key !== "readmeFilename" && index % 2 !== 0
      )
      .map(key => getItem(key, state.data[key]))
};

export default getters;

function getItem(key, objVal) {
  let itemVal = getItemValue(key, objVal);
  return {
    key,
    label: capitalize(key.replace("_", "")),
    icon: getItemDisplayIcon(key, objVal),
    iconColor: getRandomColor(),
    type: getItemType(itemVal),
    value: itemVal,
    alwaysVisible: ["name", "description", "homepage", "author"].includes(key)
  };
}

function getItemType(val) {
  if (isArray(val)) {
    return "array";
  } else if (isString(val)) {
    return "string";
  } else {
    return "";
  }
}

function getItemDisplayIcon(key, val) {
  switch (key) {
    case "versions":
      return "history";
    case "author":
      return "person";
    case "users":
      return "people";
    case "description":
      return key;
    case "_id":
      return "face";
    case "repository":
      return "developer_board";
    case "bugs":
      return "bug_report";
    case "maintainers":
    case "contributors":
      return "build";
    case "homepage":
      return "home";
    case "time":
      return "access_time";
    case "keywords":
      return "vpn_key";
    default:
      break;
  }

  if (isString(val)) {
    return "note";
  } else if (isArray(val)) {
    return "view_array";
  } else {
    return "description";
  }
}

function getItemValue(key, val) {
  if (isString(val)) {
    return val;
  } else if (isArray(val)) {
    switch (key) {
      case "maintainers":
      case "contributors":
        return val.map(item => ({
          name: item.email,
          color: "primary",
          textColor: "white",
          icon: "account_circle"
        }));
      case "keywords":
        return val.map(item => ({
          name: item,
          color: "primary",
          textColor: "white",
          icon: ""
        }));
      default:
        return [];
    }
  } else {
    switch (key) {
      case "bugs":
      case "repository":
        return val.url;
      case "author":
        return val.name;
      case "versions":
        return val.version;
      case "users":
        return Object.keys(val)
          .filter(k => val[k])
          .slice(0, 5)
          .map(item => ({
            name: item,
            color: "primary",
            textColor: "white",
            icon: "account_circle"
          }));
      case "time":
        return ["modified", "created"].map(k => {
          let icon = "";
          if (k === "modified") {
            icon = "mode_edit";
          } else {
            icon = "add";
          }
          return {
            name: new Date(val[k]).toLocaleDateString(),
            color: "primary",
            textColor: "white",
            icon
          };
        });
      default:
        return JSON.stringify(val);
    }
  }
}
