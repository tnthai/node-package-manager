import Vue from "vue";
import Component from "vue-class-component";
import { mapActions, mapGetters } from "vuex";
import { PackageModule } from "../../constants";
import { SelectedItemPanel } from "./selected-item";
import { PackageInfoDialog } from "../package-info";
import { isEmpty } from "lodash";

const { Local, Ui } = PackageModule;
@Component({
  template: require("./local-package.html"),
  methods: mapActions({
    addPackageFolder: `${Local}/addPackageFolder`,
    fetchPackageFolders: `${Local}/fetchPackageFolders`,
    selectPkg: `${Local}/selectPkg`
  }),
  computed: mapGetters({
    localPkgItems: `${Local}/localPackageItems`,
    isAppLoading: `${Local}/isAppLoading`,
    selectedPkg: `${Local.SelectedItem}/item`
  }),
  components: {
    "selected-item-panel": SelectedItemPanel,
    "detail-dialog": PackageInfoDialog
  }
})
export class LocalPackage extends Vue {
  tabModel = "tab-general";

  get pkgDetailPanelVisible() {
    return !isEmpty(this.selectedPkg);
  }

  get navBarMenus() {
    return this.$store.getters[`${Ui}/toolbarMenus`](Local, this);
  }

  rowsPerPageItems = [4, 8, 12];
  pagination = {
    rowsPerPage: 6
  };

  mounted() {
    this.fetchPackageFolders();
  }

  addLocalPackage() {
    this.addPackageFolder();
  }

  onSelectPackage(name) {
    this.selectPkg(name);
  }
}
