import Vue from "vue";
import Component from "vue-class-component";
import { PackageModule } from "../../../constants";
import { mapGetters, mapActions } from "vuex";

const { Local, PackageInfo } = PackageModule;

@Component({
  template: require("./selected-item.html"),
  computed: mapGetters({
    selectedPackage: `${Local}/selectedPackage`,
    selectedPkgDeps: `${Local.SelectedItem}/displayDependencies`,
    selectedPkgScripts: `${Local.SelectedItem}/displayScripts`,
    uninstallingDep: `${Local.SelectedItem}/uninstallingDep`,
    executingScript: `${Local.SelectedItem}/executingScript`,
    removingScript: `${Local.SelectedItem}/removingScript`,
    depSearchCriteria: `${Local.SelectedItem}/depFilterString`,
    scriptSearchCriteria: `${Local.SelectedItem}/scriptFilterString`,
    depSearchTypes: `${Local.SelectedItem}/depFilterTypes`,
    depFilterTypeItems: `${Local.SelectedItem}/depFilterTypeItems`,
    depFilterNeedUpdate: `${Local.SelectedItem}/depFilterNeedUpdate`,
    isAppLoading: `${Local.SelectedItem}/isAppLoading`,
    scriptsTableHeaders: `${Local.SelectedItem}/scriptsTableHeaders`,
    depsTableHeaders: `${Local.SelectedItem}/depsTableHeaders`
  }),
  methods: mapActions({
    fetchPkgInfo: `${PackageInfo}/fetchPackageInfo`,
    togglePkgInfoDialog: `${PackageInfo}/toggleDialog`,
    unSelectPkg: `${Local}/unselectPkg`,
    removeSelectedPackage: `${Local}/removeSelectedPackage`,
    refresh: `${Local}/reloadSelectedFolderPackage`,
    uninstallPackage: `${Local.SelectedItem}/uninstallPackage`,
    setDepSearchCriteria: `${Local.SelectedItem}/setDepSearchCriteria`,
    setScriptSearchCriteria: `${Local.SelectedItem}/setScriptSearchCriteria`,
    setDepSearchTypes: `${Local.SelectedItem}/setDepSearchTypes`,
    setDepNeedUpdateFilter: `${Local.SelectedItem}/setDepNeedUpdateFilter`,
    executeScript: `${Local.SelectedItem}/executeScript`,
    removeScript: `${Local.SelectedItem}/removeScript`,
    installDep: `${Local.SelectedItem}/installDependency`
  })
})
export class SelectedItemPanel extends Vue {
  tabModel = "tab-deps";

  pagination = {
    sortBy: "name"
  };

  rowsPerPageItems = [7, 10, { text: "All", value: -1 }];

  get depSearchCriteriaModel() {
    return this.depSearchCriteria;
  }

  set depSearchCriteriaModel(val) {
    this.setDepSearchCriteria(val);
  }

  get scriptSearchCriteriaModel() {
    return this.scriptSearchCriteria;
  }

  set scriptSearchCriteriaModel(val) {
    this.setScriptSearchCriteria(val);
  }

  get depSearchTypesModel() {
    return this.depSearchTypes;
  }

  set depSearchTypesModel(val) {
    return this.setDepSearchTypes(val);
  }

  get depFilterNeedUpdateModel() {
    return this.depFilterNeedUpdate;
  }

  set depFilterNeedUpdateModel(val) {
    this.setDepNeedUpdateFilter(val);
  }

  onUnSelectPackage() {
    this.unSelectPkg();
  }

  onUpgradePackage(name) {}

  onUninstallPackage(pkgName, type) {
    this.uninstallPackage({ pkgName, type });
  }

  onViewDetail(name) {
    this.togglePkgInfoDialog();
    this.fetchPkgInfo(name);
  }

  onRemovePackage() {
    this.removeSelectedPackage();
  }

  isUpgradeBtnDisabled(depName) {
    return this.$store.getters[`${Local.SelectedItem}/isDepUpgradeDisabled`](
      depName
    );
  }

  onRefresh() {
    this.refresh();
  }

  onExecuteScript(name) {
    this.executeScript(name);
  }

  onRemovingScript(name) {
    this.removeScript(name);
  }

  onInstallDep(name) {
    console.log("Ready to install ", name);
    this.installDep(name);
  }
}
