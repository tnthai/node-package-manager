import Vue from "vue";
import Component from "vue-class-component";
import { mapGetters, mapActions } from "vuex";
import { PackageModule } from "../../constants";

const { PackageInfo } = PackageModule;
@Component({
  template: require("./package-info.html"),
  computed: mapGetters({
    visible: `${PackageInfo}/visible`,
    loading: `${PackageInfo}/loading`,
    data: `${PackageInfo}/data`,
    readmeHtml: `${PackageInfo}/getReadmeAsHtml`,
    evenPackageInfo: `${PackageInfo}/evenInfos`,
    oddPackageInfo: `${PackageInfo}/oddInfos`
  }),
  methods: mapActions({
    toggleDialog: `${PackageInfo}/toggleDialog`
  })
})
export class PackageInfoDialog extends Vue {
  tabModel = "tab-readme";

  onDialogClose() {
    this.toggleDialog();
  }

  beforeDestroy() {
    this.toggleDialog(true);
  }
}
