import Vue from "vue";
import Component from "vue-class-component";
import { mapActions, mapGetters } from "vuex";
import { shell } from "electron";
import { getPackageHomePage } from "utils";
import { PackageModule } from "../../constants";
import { PackageInfoDialog } from "../package-info";
import { ProjectSelectionDialog } from "./project-selection-dialog";

const { NpmMarket, PackageInfo, Ui } = PackageModule;

@Component({
  template: require("./market-package.html"),
  methods: mapActions({
    searchPackageOnline: `${NpmMarket}/searchPackageOnline`,
    installPackage: `${NpmMarket}/installPackage`,
    setDisplayFields: `${NpmMarket}/setDisplayFields`,
    setFilter: `${NpmMarket}/setFilter`,
    fetchPackageInfo: `${PackageInfo}/fetchPackageInfo`,
    togglePkgInfoDialog: `${PackageInfo}/toggleDialog`
  }),
  computed: mapGetters({
    searchedPackageResult: `${NpmMarket}/searchedPackageResult`,
    searchedPackageFilter: `${NpmMarket}/searchedPackageFilter`,
    isLoading: `${NpmMarket}/isLoading`,
    tableHeaders: `${NpmMarket}/tablePkgHeaders`,
    totalPages: `${NpmMarket}/resultTotalPages`
  }),
  components: {
    "detail-dialog": PackageInfoDialog,
    "project-selection-dialog": ProjectSelectionDialog
  }
})
export class MarketPackage extends Vue {
  selectedPackageName = "";
  get navBarMenus() {
    return this.$store.getters[`${Ui}/toolbarMenus`](NpmMarket, this);
  }

  onSearchInputEnter(e) {
    this.searchPackageOnline(1);
  }

  installPackageFromMarket(item, isGlobal) {
    this.installPackage({
      name: item.name,
      isGlobal
    });

    this.selectedPackageName = item.name;
  }

  onPageChange(page) {
    console.log("We're on page: ", page);
    this.searchPackageOnline(page);
  }

  goToPackagePage(name) {
    shell.openExternal(getPackageHomePage(name));
  }

  get selectedDisplayFields() {
    return this.searchedPackageFilter.selectedDisplayFields;
  }

  set selectedDisplayFields(val) {
    this.setDisplayFields(val);
  }

  get packageQuery() {
    return this.searchedPackageFilter.query;
  }

  set packageQuery(val) {
    this.setFilter(val);
  }

  viewPackageDetail(name) {
    this.togglePkgInfoDialog();
    this.fetchPackageInfo(name);
  }

  onSettingsClick(e) {}
}
