import Vue from "vue";
import Component from "vue-class-component";
import { PackageModule } from "../../../constants";
import { mapActions, mapGetters } from "vuex";

const { NpmMarket, Local } = PackageModule;
@Component({
  template: require("./dialog.html"),
  methods: mapActions({
    toggleProjectSelectionDialog: `${NpmMarket}/toggleProjectSelectionDialog`,
    installPackage: `${NpmMarket}/installPackage`
  }),
  computed: mapGetters({
    localProjects: `${Local}/localPackagePathItems`,
    isLoadingLocalProjects: `${Local}/isAppLoading`,
    prjSelectionDialogVisible: `${NpmMarket}/isPrjSelectionDialogVisible`
  }),
  props: {
    packageName: String
  }
})
export class ProjectSelectionDialog extends Vue {
  localPrjCheckboxModel = {};

  onLocalPkgDialogClose(e) {
    this.toggleProjectSelectionDialog();
  }

  onSelectLocalProjectSave(e) {
    let selectedProjects = this.localProjects
      .filter(prj => this.localPrjCheckboxModel[prj.name])
      .map(prj => {
        let installOptions = "";
        if (this.localPrjCheckboxModel[`${prj.name}_dep`]) {
          installOptions = "--save";
        } else if (this.localPrjCheckboxModel[`${prj.name}_devdep`]) {
          installOptions = "--save-dev";
        } else if (this.localPrjCheckboxModel[`${prj.name}_optdep`]) {
          installOptions = "--save-optional";
        } else {
          installOptions = "--no-save";
        }

        return {
          name: prj.name,
          path: prj.path.replace("package.json", ""),
          installOptions
        };
      });

    this.installPackage({
      name: this.packageName,
      isGlobal: false,
      selectedProjects
    });

    this.toggleProjectSelectionDialog();
  }
}
