import Vue from "vue";
import Component from "vue-class-component";
import { mapGetters, mapActions } from "vuex";
import { initRightClickContextMenu } from "utils";
import { PackageModule } from "../../constants";

const { Ui } = PackageModule;

@Component({
  name: "app-container",
  template: require("./appContainer.html"),
  computed: mapGetters({
    isAppLoading: `${Ui}/isAppLoading`,
    isDrawerVisible: `${Ui}/isDrawerVisible`,
    menuItems: `${Ui}/drawerMenuItems`
  }),
  methods: mapActions({
    toggleAppDrawer: `${Ui}/toggleAppDrawer`,
    getUserDataPath: `${Ui}/getUserDataPath`,
    initResizeEvents: `${Ui}/initResizeEvents`
  })
})
export class AppContainer extends Vue {
  mounted() {
    initRightClickContextMenu();
    this.getUserDataPath();
    this.initResizeEvents();
  }

  goToPage(page) {
    if (!page) {
      return;
    }

    this.$router.push(page);
  }

  onAppDrawerClick(e) {
    this.toggleAppDrawer();
  }
}
