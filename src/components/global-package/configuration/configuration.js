import Vue from "vue";
import Component from "vue-class-component";
import { mapGetters, mapActions } from "vuex";
import { PackageModule } from "../../../constants";

const { Configuration } = PackageModule.Global;

@Component({
  template: require("./configuration.html"),
  computed: mapGetters({
    globalPackageConfig: `${Configuration}/data`,
    _selectedDisplayFields: `${Configuration}/selectedDisplayFields`,
    totalPages: `${Configuration}/npmConfigTotalPages`,
    configItems: `${Configuration}/npmConfigItems`
  }),
  methods: mapActions({
    toggleGlobalConfiguration: `${Configuration}/toggleConfigDialog`,
    setGlobalPackageDisplayFields: `${Configuration}/setDisplayFields`,
    updateGlobalConfigPageIndex: `${Configuration}/updateConfigPageIndex`,
    updateGlobalConfigItem: `${Configuration}/updateConfigItem`,
    saveGlobalNpmConfig: `${Configuration}/saveNpmConfig`,
    fetchGlobalNpmConfig: `${Configuration}/fetchNpmConfig`
  })
})
export class GlobalNpmConfigDialog extends Vue {
  tabModel = "tab-general";

  get selectedDisplayFields() {
    return this._selectedDisplayFields;
  }

  set selectedDisplayFields(val) {
    this.setGlobalPackageDisplayFields(val);
  }

  onPageChange(e) {
    this.updateGlobalConfigPageIndex(e);
  }

  onConfigurationSave(e) {
    console.log("On configuration saving ...");
    this.saveGlobalNpmConfig();
  }

  onConfigChange(e, itemKey) {
    console.log("On configuration item value change ", e, itemKey);
    this.updateGlobalConfigItem({ key: itemKey, newVal: e });
  }

  onConfigurationClose(e) {
    this.toggleGlobalConfiguration();
  }

  onConfigurationRefresh(e) {
    this.fetchGlobalNpmConfig();
  }
}
