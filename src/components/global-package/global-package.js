import Vue from "vue";
import Component from "vue-class-component";
import { mapActions, mapGetters } from "vuex";
import { shell } from "electron";
import { GlobalNpmConfigDialog } from "./configuration";
import { PackageInfoDialog } from "../package-info";
import { isArray } from "lodash";
import { PackageModule } from "../../constants";

const { Global, PackageInfo, Ui } = PackageModule;

@Component({
  template: require("./global-package.html"),
  methods: mapActions({
    fetchPackages: `${Global}/fetchPackages`,
    uninstallPackage: `${Global}/uninstallPackage`,
    upgradePackage: `${Global}/upgradePackage`,
    setPackageFilter: `${Global}/setPackageFilter`,
    toggleConfigDialog: `${Global.Configuration}/toggleConfigDialog`,
    fetchNpmConfig: `${Global.Configuration}/fetchNpmConfig`,
    fetchPackageInfo: `${PackageInfo}/fetchPackageInfo`,
    togglePkgInfoDialog: `${PackageInfo}/toggleDialog`
  }),
  computed: mapGetters({
    searchFilterString: `${Global}/searchFilterString`,
    displayItems: `${Global}/displayItems`,
    tableHeaders: `${Global.Configuration}/tablePkgHeaders`,
    tableColumns: `${Global.Configuration}/tableColumns`,
    tableLoading: `${Global}/loading`
  }),
  components: {
    "configuration-dialog": GlobalNpmConfigDialog,
    "detail-dialog": PackageInfoDialog
  }
})
export class GlobalPackage extends Vue {
  pagination = {
    sortBy: "name"
  };

  rowsPerPageItems = [7, 10, { text: "All", value: -1 }];

  get navBarMenus() {
    return this.$store.getters[`${Ui}/toolbarMenus`](Global, this);
  }

  mounted() {
    this.fetchPackages();
  }

  changeSort(column) {
    if (this.pagination.sortBy === column) {
      this.pagination.descending = !this.pagination.descending;
    } else {
      this.pagination.sortBy = column;
      this.pagination.descending = false;
    }
  }

  onUpgradePackage(name) {
    this.upgradePackage(name);
  }

  onUninstallPackage(name) {
    this.uninstallPackage(name);
  }

  goToPackagePage(url) {
    shell.openExternal(url);
  }

  refreshGlobalPackages() {
    this.fetchPackages();
  }

  get searchCriteria() {
    return this.searchFilterString;
  }

  set searchCriteria(val) {
    this.setPackageFilter(val);
  }

  getDataCellDisplayValue(val) {
    if (isArray(val)) {
      return val.join(", ");
    }

    return val;
  }

  onSettingsClick(e) {
    this.toggleConfigDialog();
    this.fetchNpmConfig();
  }

  onViewDetail(name) {
    this.togglePkgInfoDialog();
    this.fetchPackageInfo(name);
  }
}
