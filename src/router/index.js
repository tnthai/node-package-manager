import Vue from "vue";
import VueRouter from "vue-router";

// components
import { Dashboard } from "../components/dashboard";
import { GlobalPackage } from "../components/global-package";
import { MarketPackage } from "../components/market-package";
import { LocalPackage } from "../components/local-package";

Vue.use(VueRouter);

const router = new VueRouter({
  // mode: "history",
  routes: [
    {
      component: Dashboard,
      name: "index",
      path: "/"
    },
    {
      component: GlobalPackage,
      name: "global-packages",
      path: "/packages/global"
    },
    {
      component: LocalPackage,
      name: "local-packages",
      path: "/packages/local"
    },
    {
      component: MarketPackage,
      name: "market",
      path: "/market"
    }
  ]
});

export default router;
