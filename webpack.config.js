const path = require("path");
const webpack = require("webpack");
const nodeExternals = require("webpack-node-externals");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const WebpackBundleSizeAnalyzerPlugin = require("webpack-bundle-size-analyzer")
  .WebpackBundleSizeAnalyzerPlugin;
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const SRC_DIR = path.resolve(__dirname, "src");
const BUILD_DIR = path.resolve(__dirname, "dist");

module.exports = env => ({
  // externals: {
  //   path: "path",
  //   url: "url"
  // },
  target: "electron",
  node: {
    __dirname: false,
    __filename: false
  },
  externals: [
    nodeExternals({
      whitelist: [
        "vue",
        "vuetify",
        "vuex",
        "vuex-router-sync",
        "vue-class-component",
        "vue-router",
        "vue-property-decorator"
      ]
    })
  ],
  entry: {
    app: SRC_DIR + "/main.js",
    background: SRC_DIR + "/background.js"
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].js"
  },
  devtool: "eval-source-map",
  resolve: {
    extensions: [".js", ".html", ".json"],
    alias: {
      vue: "vue/dist/vue.js"
    },
    modules: [path.resolve("node_modules"), path.resolve(SRC_DIR)]
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.jsx?$/,
        loader: "eslint-loader",
        exclude: /node_modules/
      },
      {
        test: /\.jsx?$/,
        loader: "babel-loader"
      },
      {
        test: /\.html$/,
        loader: "raw-loader",
        exclude: [SRC_DIR + "/index.html"]
      },
      {
        test: /\.s?css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(env)
    }),
    new HtmlWebpackPlugin({
      template: SRC_DIR + "/index.html",
      excludeChunks: ["background"]
    }),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, "assets"), to: BUILD_DIR }
    ]),
    new webpack.LoaderOptionsPlugin({
      test: /\.jsx?$/,
      options: {
        eslint: {
          emitError: true,
          emitWarning: false,
          quiet: true,
          fix: false,
          failOnError: true,
          failOnWarning: false
        }
      }
    }),
    new ExtractTextPlugin({
      filename: "[name].css",
      allChunks: true
    }),
    new FriendlyErrorsWebpackPlugin({ clearConsole: env === "development" }),
    new WebpackBundleSizeAnalyzerPlugin(BUILD_DIR + "/bundle-report.txt"),
    new CleanWebpackPlugin(["dist"])
  ],
  devServer: {
    contentBase: BUILD_DIR,
    compress: false,
    port: 8080,
    historyApiFallback: true
  }
});
